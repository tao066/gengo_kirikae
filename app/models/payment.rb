class Payment < ApplicationRecord
  belongs_to :user
  belongs_to :good

  def sum
    if !self.good.nil? && !self.good.price.nil? || !self.number
      return self.good.price * self.number
    else
      return false
    end
  end
end
