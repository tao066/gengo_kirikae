class User < ApplicationRecord
  # Others available are:
  # :recoverable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :rememberable, :trackable, :validatable

  validates :name, presence: true, length: { maximum: 50 }

  has_many :payments

end
