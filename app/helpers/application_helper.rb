module ApplicationHelper
  # alert => danger, notice => success
  def alert_class(message_type)
    case message_type
      when "alert"
        return "danger"
      when "notice"
        return "success"
      else
        return message_type
    end
  end

  def default_link_nav(str, path)
    return link_to(str, path, class: "nav-item nav-link")
  end

  def logout_link_nav(str, path, text)
    return link_to(str, path, method: "delete", data: { confirm: text }, class: "nav-item nav-link") 
  end

  def costom_devise_error_messages
    return "" if resource.errors.empty?
    html = ""
    messages = resource.errors.full_messages.each do |msg|
      html += <<-EOF
      <div class="alert alert-danger">#{msg}</div>
      EOF
    end
    html.html_safe
  end

  def locale_url_for(locale, str)
    if locale == I18n.default_locale.to_s
      link_to_if(
        params[:locale].present? && params[:locale] != locale, str,
        url_for(
          controller: controller.controller_name,
          action: controller.action_name,
          locale: ''
        )
      )
    else
      link_to_if(
        params[:locale] != locale, str,
        url_for(
          controller: controller.controller_name,
          action: controller.action_name,
          locale: locale
        )
      )
    end
  end
end
