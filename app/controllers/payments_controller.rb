class PaymentsController < ApplicationController
  def create
    @payment = Payment.new(permit_payment)

    @good ||= Good.find_by(id: @payment.good_id)
    if @good.nil?
      return redirect_to(goods_path, alert: "この商品は存在しません")
    end

    value = 0

    if @payment.sum
      value = current_user.money - @payment.sum
      if value < 0
        flash.now[:alert] = "いまの所持金では、この商品を購入できません"
        return render("/goods/show")
      end
    end

    if @payment.save
      current_user.update_attribute(:money, value)
      value = @good.number - @payment.number
      @good.update_attribute(:number, value)
      redirect_to @payment, notice: "商品を購入しました"
    else
      render "/goods/show"
    end
  end

  def show
    @payment ||= Payment.find_by(id: params[:id])
    if @payment.nil? || @payment.user_id != current_user.id
      return redirect_to(mypage_path, alert: "この商品明細は存在しません")
    end
  end

  private

  def permit_payment
    params[:payment][:user_id] = current_user.id

    params.require(:payment).permit(
      :number, :user_id, :good_id
    )
  end
end