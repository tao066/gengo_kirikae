class PayCodesController < ApplicationController
  def new
    @pay_code = PayCode.new
  end

  def create
    @pay_code ||= PayCode.find_by(key: params[:pay_code][:key].upcase)
    if @pay_code.nil?
      flash.now[:alert] = "このコードは存在しません"
      @pay_code = PayCode.new(key: params[:pay_code][:key])
      render 'pay_codes/new'
    else
      current_user.update_attribute(:money, current_user.money + @pay_code.price)
      @pay_code.destroy
      flash[:notice] = "入金を完了しました"
      redirect_to mypage_path
    end
  end
end
