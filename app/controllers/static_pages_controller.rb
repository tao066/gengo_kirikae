class StaticPagesController < ApplicationController
  def home
  end

  def mypage
    @user = current_user
    @payments = @user.payments.order("id DESC")
  end
end
