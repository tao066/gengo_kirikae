class GoodsController < ApplicationController
  def index
    @goods = Good.all
  end

  def show
    @good ||= Good.find_by(id: params[:id])
    if @good.nil?
      return redirect_to(goods_path, alert: "この商品は存在しません")
    end
    @payment = Payment.new
    value = current_user.money / @good.price
    @max = value < @good.number ? value : @good.number
  end
end
