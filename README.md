# Rails：言語切り替え

- 各自でRailsの動作環境を構築してください。

```
$ bundle install --path vendor/bundle
```

- マイグレーションファイルが用意されているので、マイグレーションしてデータベースを構築してください。

```
$ bundle exec rails db:create
$ bundle exec rails db:migrate
```

- seeds を一回のみ実行してください。その後ログに出てくる Key をメモしてください。

```
$ bundle exec rails db:seed
```

- 完了したら、サーバを起動させて挙動を確認してください。

```
$ bundle exec rails server
```
