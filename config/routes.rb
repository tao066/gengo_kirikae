Rails.application.routes.draw do
  scope "(:locale)" do
    # staic pages
    root           to: 'static_pages#home'
    get '/mypage', to: 'static_pages#mypage'

    # devise
    devise_for :users

    # goods
    resources :goods, only: [:index, :show]

    # payments
    resources :payments, only: [:show, :create]

    # pay codes
    resources :pay_codes, only:[:new, :create]
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
