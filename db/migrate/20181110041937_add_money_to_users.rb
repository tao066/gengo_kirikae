class AddMoneyToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :money, :integer, null: false, default: 0
  end
end
