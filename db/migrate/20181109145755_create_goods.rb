class CreateGoods < ActiveRecord::Migration[5.1]
  def change
    create_table :goods do |t|
      t.string  :name,   null: false, default: ""
      t.integer :price , null: false, default: 0
      t.integer :number, null: false, default: 0

      t.timestamps
    end
  end
end
