class CreatePayments < ActiveRecord::Migration[5.1]
  def change
    create_table :payments do |t|
      t.integer    :number, null: false, default: 0
      t.references :user
      t.references :good

      t.timestamps
    end
  end
end
