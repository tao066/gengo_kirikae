class CreatePayCodes < ActiveRecord::Migration[5.1]
  def change
    create_table :pay_codes do |t|
      t.string  :key,   null: false, default: ""
      t.integer :price, null: false, default: 0

      t.timestamps
    end

    add_index :pay_codes, :key, unique: true
  end
end
