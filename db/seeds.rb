# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


## create goods
require 'csv'

print "goods data import start"

csv_data = CSV.read('./db/goods.csv', headers: true)
goods = []

csv_data.each do |data|
  print "."
  goods << Good.new(name: data['name'], price: data['price'], number: data['number'])
end

Good.import goods

print "\nfinished!\n"
## ----------------

## create pay code
require 'securerandom'

print "pay codes create start"

pay_codes = []
code_box  = []

10.times do |i|
  print "."
  code_box  << [SecureRandom.hex(32)[0..15].upcase, [500, 1000, 1500].sample]
  pay_codes << PayCode.new(key: code_box[i][0], price: code_box[i][1])
end

PayCode.import pay_codes
print "\nfinished!\n"

code_box.each_with_index do |line, i|
  puts "[#{i}] key:#{line[0]} price:#{line[1]}"
end
## ----------------
