require 'test_helper'

class PayCodesControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get pay_codes_new_url
    assert_response :success
  end

  test "should get result" do
    get pay_codes_result_url
    assert_response :success
  end

end
